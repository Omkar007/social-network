//importing modules
const User=require('../models/user');
const express = require('express');
const router = express.Router();
const Blog=require('../models/blog');
const jwt=require('jsonwebtoken');// Compact, URL-safe means of representing claims to be transferred between two parties.
const config=require('../../config/database');

//copied middleware from userroutes ---> Not able to recognize from userroute.js file
router.use((req,res,next)=>{
const token=req.headers['authorization'];
if(!token){
  res.json({success:false,message:'No token provided'});
}
else{
  jwt.verify(token,config.secret,(err,decoded)=>{
    if(err){
  res.json({success:false,message:'Token is invalid'});
    }
    else{
      req.decoded=decoded;  //this can be accessed in any routes written below
      next();
    }
  });
}
});

//create new blog method 
router.post('/newBlog',(req,res)=>{

if(!req.body.title){
 res.json({success:false, message: 'Blog Title cannot be empty. Please Enter Blog Title!!'});
}
else{
     if(!req.body.body){
        res.json({success:false, message: 'Blog Body cannot be empty. Please enter the Blog content!!'});
    }
    else{
        if(!req.body.createdBy){
        res.json({success:false, message: 'Please enter CreatedBy. This field is mandatory!!'});
        }
        else{
            let blog=new Blog({
            title:req.body.title,
            body:req.body.body,
            createdBy:req.body.createdBy,
        });
        
        blog.save((err)=>{
            if(err){
                if(err.errors){
                    if(err.errors.title){
                       res.json({success:false, message: err.errors.title.message});
                    }
                    else if(err.errors.body){
                       res.json({success:false, message: err.errors.body.message});
                    }
                    else{
                      res.json({success:false, message: err.errmsg});
                    }
                }
                else{
                       res.json({success:false, message: err});
                } 
            }
            else{
            //success
              res.json({success:true, message: 'Blog Saved!'});
            }
        });
        }
    
    }
}

});

//retrieve all blogs
router.get('/allBlogs',(req,res)=>{
    Blog.find({},(err,blogs)=>{
        if(err){
          res.json({success:false, message: err});
        }
        else if(!blogs){
          res.json({success:false, message: 'No Blogs Found !'});
        }
        else{
              res.json({success:true, blogs:blogs});
        }
    }).sort({'_id':-1});  //-1 defines descending (new posts at top)
});

//get single blog
router.get('/singleBlog/:id',(req,res)=>{
    if(!req.params.id){
            res.json({success:false,message:'No such Blog Id found'});
    }
    else{
    Blog.findOne({_id:req.params.id},(err,blog)=>{
        if(err){
            res.json({success:false,message:err});
        }
        else{
            if(!blog){
        res.json({success:false,message:'Blog not Found !'});
            }
            else{
        res.json({success:false,blog:blog});
            }
        }
    });
    }
});

//api to update blog 
router.put('/updateBlog',(req,res)=>{
    if(!req.body._id){
            res.json({success:false,message:'Blog id not provided !'});
    }
    else{
    Blog.findOne({_id:req.body._id},(err,blog)=>{
        if(err){
              res.json({success:false,message:err});
        }
        else{
            if(!blog){
             res.json({success:false,message:'No such Blog found !'});
            }
            else{

                 User.findOne({ _id:req.decoded.userId }, (err, user) => {
                    if(err){
                             res.json({success:false,message:err});
                    }
                    else{
                        if(user.username!==req.body.createdBy){
                             res.json({success:false,message:'You are not authorized to edit this blog!'});
                        }
                        else{
                            blog.title=req.body.title;
                            blog.body=req.body.body;
                            blog.save((err)=>{
                                if(err){
                                      res.json({success:false,message:'Exception occurred in save editBlog '+err});
                                }
                                else{
                                     res.json({success:true,message:'Blog Updated!'});
                                }
                            });
                        }
                    }
                });

            }
        }
    });
    }

});

//api to delete blog 
router.delete('/deleteBlog/:id',(req,res)=>{

    if(!req.params.id){
      res.json({success:false,message:'Blog id required for deleting Blog!'});
    }
    else{
        Blog.findOne({_id:req.params.id},(err,blog)=>{
            if(err){
      res.json({success:false,message:err});
            }
            else{

                User.findOne({_id:req.decoded.userId},(err,user)=>{

                    if(err){
                res.json({success:false,message:err});
                    }
                    else{

                        if(user.username!==blog.createdBy){
                res.json({success:false,message:'User is not authorized to delete Post'});
                        }
                        else{
                            blog.remove((err)=>{
                                if(err){
                     res.json({success:false,message:err});
                                }
                                else{
                    res.json({success:true,message:'Blog Deleted !'});
                                }
                            });
                        }

                    }

                });
            }
        });
    }

});

//likes blog api
router.put('/likesBlog',(req,res)=>{
        if(!req.body.id){
       res.json({success:false,message:'No Blog is provided!'});
        }
        else{
            Blog.findOne({_id:req.body.id},(err,blog)=>{
                if(err){
                    res.json({success:false,message:err});
                }
                else{
                    if(!blog){
                     res.json({success:false,message:"Blog was not found!"});
                    }
                    else{
                        User.findOne({_id:req.decoded.userId},(err,user)=>{
                            if(err){
                                res.json({success:false,message:err});
                            }
                            else{
                                if(!user){
                            res.json({success:false,message:"User was not found!"});
                                }
                                else if(user.username===blog.createdBy){
                            res.json({success:false,message:"User cannot like his own Post!"});
                                }
                                else{

                            if(blog.likedBy.includes(user.username)){
                                res.json({success:false,message:"You already liked this Post!"});
                            }
                            else{
                                 if(blog.dislikedBy.includes(user.username)){
                                    blog.dislikes--;
                                    let arrIndex=blog.dislikedBy.indexOf(user.username);
                                    blog.dislikedBy.splice(arrIndex,1);
                                    blog.likedBy.push(user.username);
                                    blog.likes++;
                                    blog.save((err)=>{
                                        if(err){
                                            res.json({success:false,message:err});
                                        }
                                        else{
                                             res.json({success:true,message:'You have liked this Post!'});
                                        }
                                    });
                                 }
                                 else{
                                    blog.likedBy.push(user.username);
                                    blog.likes++;
                                    blog.save((err)=>{
                                        if(err){
                                            res.json({success:false,message:err});
                                        }
                                        else{
                                             res.json({success:true,message:'You have liked this Post!'});
                                        }
                                    });
                                 }
                            }
                            }
                            }
                        });

                    }
                }

            });
        }
});

//dislike API
router.put('/dislikeBlog', (req, res) => {
    // Check if id was provided inside the request body
    if (!req.body.id) {
      res.json({ success: false, message: 'No id was provided.' }); // Return error message
    } else {
      // Search database for blog post using the id
      Blog.findOne({ _id: req.body.id }, (err, blog) => {
        // Check if error was found
        if (err) {
          res.json({ success: false, message: 'Invalid blog id' }); // Return error message
        } else {
          // Check if blog post with the id was found in the database
          if (!blog) {
            res.json({ success: false, message: 'That blog was not found.' }); // Return error message
          } else {
            // Get data of user who is logged in
            User.findOne({ _id: req.decoded.userId }, (err, user) => {
              // Check if error was found
              if (err) {
                res.json({ success: false, message: 'Something went wrong.' }); // Return error message
              } else {
                // Check if user was found in the database
                if (!user) {
                  res.json({ success: false, message: 'Could not authenticate user.' }); // Return error message
                } else {
                  // Check if user who disliekd post is the same person who originated the blog post
                  if (user.username === blog.createdBy) {
                    res.json({ success: false, messagse: 'Cannot dislike your own post.' }); // Return error message
                  } else {
                    // Check if user who disliked post has already disliked it before
                    if (blog.dislikedBy.includes(user.username)) {
                      res.json({ success: false, message: 'You already disliked this post.' }); // Return error message
                    } else {
                      // Check if user has previous disliked this post
                      if (blog.likedBy.includes(user.username)) {
                        blog.likes--; // Decrease likes by one
                        const arrayIndex = blog.likedBy.indexOf(user.username); // Check where username is inside of the array
                        blog.likedBy.splice(arrayIndex, 1); // Remove username from index
                        blog.dislikes++; // Increase dislikeds by one
                        blog.dislikedBy.push(user.username); // Add username to list of dislikers
                        // Save blog data
                        blog.save((err) => {
                          // Check if error was found
                          if (err) {
                            res.json({ success: false, message: 'Something went wrong.' }); // Return error message
                          } else {
                            res.json({ success: true, message: 'Blog disliked!' }); // Return success message
                          }
                        });
                      } else {
                        blog.dislikes++; // Increase likes by one
                        blog.dislikedBy.push(user.username); // Add username to list of likers
                        // Save blog data
                        blog.save((err) => {
                          // Check if error was found
                          if (err) {
                            res.json({ success: false, message: 'Something went wrong.'+err }); // Return error message
                          } else {
                            res.json({ success: true, message: 'Blog disliked!' }); // Return success message
                          }
                        });
                      }
                    }
                  }
                }
              }
            });
          }
        }
      });
    }
  });

//comment api 
router.post('/comment',(req,res)=>{
    if(!req.body.comment){
        res.json({ success: false, message: err });
    }
    else{
        if(!req.body.id){
            res.json({ success: false, message: 'Blog id is not provided!' });
        }
        else{
            Blog.findOne({_id:req.body.id},(err,blog)=>{
                if(err){
            res.json({ success: false, message: err });
                }
                else{
                    if(!blog){
            res.json({ success: false, message: 'Blog not found!' });
                    }
                    else{
                 User.findOne({_id:req.decoded.userId},(err,user)=>{
                    if(err){
            res.json({ success: false, message: err });
                    }
                    else{
                        if(!user){
            res.json({ success: false, message: 'User not found!' });
                        }
                        else{
                            blog.push({
                                comment:req.body.comment,
                                commentator:user.username
                            });
                            blog.save((err)=>{
                                if(err){
                                res.json({ success: false, message: err });
                                }
                                else{
                                res.json({ success: true, message: 'You have commented on post!' });
                                }
                            });
                        }
                    }
                 })  ;     

                    }
                }
            });
        }
    }


});

//exporting router
module.exports = router;