
//importing modules
const express = require('express');
const router = express.Router();
const User=require('../models/user');
const jwt=require('jsonwebtoken');// Compact, URL-safe means of representing claims to be transferred between two parties.
const config=require('../../config/database');

//function to get total no of users created
router.get('/users',(req,res)=>{
    User.find((err,users)=>{
        res.send(users);
    });
});

//function to create users and save in database
router.post('/addusers',(req,res)=>{
    //creation of user object,retrieving values from request
   var user=new User();         //getter names should match to that of schema else it will not save to db
   user.username=req.body.username.toLowerCase();
   user.password=req.body.password;
   user.email=req.body.email.toLowerCase();

   if(req.body.username==null || req.body.username=='' || req.body.password==null || req.body.password=='' ||req.body.email==null || req.body.email=='')
       {
        res.json({success:false, msg: 'Ensure that UserName, Password and Email is provided !!'});
       }
   else{
    user.save((err)=>{
   if (err) {
              // Check if error is an error indicating duplicate account
              if (err.code === 11000) {
                res.json({ success: false, message: 'Username or e-mail already exists' }); // Return error
              } else {
                // Check if error is a validation error
                if (err.errors) {
                  // Check if validation error is in the email field
                  if (err.errors.email) {
                    res.json({ success: false, message: err.errors.email.message }); // Return error
                  } else {
                    // Check if validation error is in the username field
                    if (err.errors.username) {
                      res.json({ success: false, message: err.errors.username.message }); // Return error
                    } else {
                      // Check if validation error is in the password field
                      if (err.errors.password) {
                        res.json({ success: false, message: err.errors.password.message }); // Return error
                      } else {
                        res.json({ success: false, message: err }); // Return any other error not already covered
                      }
                    }
                  }
                } else {
                  res.json({ success: false, message: 'Could not save user. Error: ', err }); // Return error if not related to validation
                }
              }
            } else {
              res.json({ success: true, message: 'Acount registered!' }); // Return success
            }
   });
   }
  });

//to check if username already exists
router.get('/checkUsername/:username',(req,res)=>{
    if(!req.params.username){
      res.json({ success: false, message: 'UserName needs to be provided '});
    }
    else{
        User.findOne({username:req.param.username},(err,user)=>{
        if (err) {
          res.json({ success: false, message: 'connection error' }); // Return connection error
        } else {
          // Check if user's UserName is taken
          if (user) {
            res.json({ success: false, message: 'UserName is already taken' }); // Return as taken UserName
          } else {
            res.json({ success: true, message: 'UserName is available' }); // Return as available UserName
          }
        }
        });
    }
});

//to check if email already exists
router.get('/checkEmail/:email',(req,res)=>{
    if(!req.params.email){
      res.json({ success: false, message: 'Email id needs to be provided '});
    }
    else{
        User.findOne({email:req.param.email},(err,user)=>{
        if (err) {
          res.json({ success: false, message: 'connection error' }); // Return connection error
        } else {
          // Check if user's e-mail is taken
          if (user) {
            res.json({ success: false, message: 'E-mail is already taken' }); // Return as taken e-mail
          } else {
            res.json({ success: true, message: 'E-mail is available' }); // Return as available e-mail
          }
        }
        });
    }
});

//to authenticate user (login)
router.post('/login',(req,res)=>{

      if(!req.body.username){
          res.json({success:false,message:'No username provided !'});
      }
      else{
        if(!req.body.password){
           res.json({success:false,message:'Please enter the password. This field is mandatory !'});
        }
        else{
          User.findOne({ username: req.body.username.toLowerCase() },(err,user)=>{
            if(err){
           res.json({success:false,message:err});
            }
            else{
              if(!user){
                res.json({ success: false, message: 'Username not found.' }); 
              }
              else{
              const validPassword=user.comparePassword(req.body.password);
              if(!validPassword){
               res.json({success:false,message:'Password provided does not match the account registered'});
              }
              else{ //json web token cant't be decrypted don't put personal info
              const token=jwt.sign({userId:user._id},config.secret,{expiresIn:'1h'});
              res.json({success:true,message:'Login successful !',token:token,user:{username:user.username}});
              }
              }
            }
          });
        }
      }
});

//authorized routes
router.use((req,res,next)=>{
const token=req.headers['authorization'];
if(!token){
  res.json({success:false,message:'No token provided'});
}
else{
  jwt.verify(token,config.secret,(err,decoded)=>{
    if(err){
  res.json({success:false,message:'Token is invalid'});
    }
    else{
      req.decoded=decoded;  //this can be accessed in any routes written below
      next();
    }
  });
}
});

router.get('/profile',(req,res)=>{
    User.findOne({ _id: req.decoded.userId }).select('username email').exec((err, user) => {
      // Check if error connecting
      if (err) {
        res.json({ success: false, message: err });
      } else {
        // Check if user was found in database
        if (!user) {
          res.json({ success: false, message: 'User not found' }); // Return error, user was not found in db
        } else {
          res.json({ success: true, user: user }); // Return success, send user object to frontend for profile
        }
      }
    });
});

router.get('/publicProfile/:username', (req, res) => {
    // Check if username was passed in the parameters
    if (!req.params.username) {
      res.json({ success: false, message: 'No username was provided' }); 
    } else {
      // Check the database for username
      User.findOne({ username: req.params.username }).select('username email').exec((err, user) => {
        // Check if error was found
        if (err) {
          res.json({ success: false, message: 'Something went wrong.' }); 
        } else {
          // Check if user was found in the database
          if (!user) {
            res.json({ success: false, message: 'Username not found.' }); 
          } else {
            res.json({ success: true, user: user });
          }
        }
      });
    }
  });

//exporting router
module.exports = router;