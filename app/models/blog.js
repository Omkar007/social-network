const mongoose=require('mongoose');       //imports the mongoose module
var Schema = mongoose.Schema;           //calls schema mehtod of mongoose

//server side validations
let titleChecker=(title)=>{
    const regex=new RegExp(/^[a-zA-Z0-9 ]+$/); 
    if(!title){
        return false;
    }
    else if(title.length<5 || title.length>50){
        return false;
    }
    else if(!regex.test(title))
          return false;
    else 
    return true;
};

let bodyChecker=(body)=>{
    if(!body){
        return false;
    }
    else if(body.length<5 || body.length>500){
        return false;
    }
    else {
          return true;
    }
};

let commentChecker=(comment)=>{
    if(!comment[0]){
        return false;
    }
    else if(comment[0].length<1 || comment[0].length>200){
        return false;
    }
    else 
    return true;
};


const titleValidators=[{
    validator:titleChecker, msg:'Title should be valid & must be atleast 5 and not greater than 50'
}];
const bodyValidators=[{
    validator:bodyChecker, msg:'Body should be atleast 5 and not greater than 500'
}];
const commentvalidator=[{
    validator:commentChecker, msg:'Comment should not be not greater than 200 characters'
}];

//creating blog schema so that document is structured this way in mongodb

const blogSchema=new Schema({
    title:{type:String,required:true,validate:titleValidators},
    body:{type:String,required:true,validate:bodyValidators},
    createdBy:{type:String},
    createdAt:{type:Date,default:Date.now()},
    likes:{type:Number,default:0},
    likedBy:{type:Array},
    dislikes:{type:Number,default:0},
    dislikedBy:{type:Array},
    comments:[                                                   //array-->[]
    {
        comment:{type:String,validate:commentvalidator},
        commentator:{type:String}
    }
    ]
});

//exporting mongodb schema
module.exports=mongoose.model('Blog',blogSchema);