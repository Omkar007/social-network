const mongoose=require('mongoose');       //imports the mongoose module
var Schema = mongoose.Schema;           //calls schema mehtod of mongoose
var bcrypt=require('bcrypt-nodejs');

//server side validations
let emailChecker=(email)=>{
    const regex=new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/); 
    if(!email){
        return false;
    }
    else if(email.length<5 || email.length>30){
        return false;
    }
    else if(!regex.test(email))
          return false;
    else 
    return true;
};

let userChecker=(username)=>{
    const regex=new RegExp(/^[a-zA-Z0-9]+$/); 
    if(!username){
        return false;
    }
    else if(username.length<5 || username.length>15){
        return false;
    }
    else if(!regex.test(username)){
          return false;
    }
    else {
          return true;
    }
};

let passwordChecker=(password)=>{
    const regex=new RegExp(/^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[\d])(?=.*?[\W]).{8,35}$/); 
    if(!password){
        return false;
    }
    else if(password.length<8 || password.length>35){
        return false;
    }
    else if(!regex.test(password))
          return false;
    else 
    return true;
};


const emailValidators=[{
    validator:emailChecker, msg:'Email should be valid & must be atleast 5 and not greater than 30'
}];
const userNameValidators=[{
    validator:userChecker, msg:'UserName should be valid & must be atleast 5 and not greater than 15'
}];
const PasswordValidators=[{
    validator:passwordChecker, msg:'Password should contain caps,lowercase charectars, numbers,special characters & must be within 8 & 35'
}];

//creating user schema so that document is structured this way in mongodb
var UserSchema=new Schema({
    username:{type: String,lowercase:true, required:true,unique:true,validate:userNameValidators},
    password:{type: String,required:true,validate:PasswordValidators},
    email:{type: String,lowercase:true, required:true,unique:true,validate:emailValidators}
});

//mongoose middleware triggers before saving to db
UserSchema.pre('save',function(next){
  var user=this; 
  //using bycrpt to encode the password 
  bcrypt.hash(user.password, null, null,function(err, hash){
      if(err){
         return next(err);
      }
     user.password=hash;
     next();
});
});

//method compares to encrypted password in database
UserSchema.methods.comparePassword=function(password){
    return bcrypt.compareSync(password,this.password);
};

//exporting mongodb schema
module.exports=mongoose.model('User',UserSchema);