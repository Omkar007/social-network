import { Component, OnInit } from '@angular/core';
import {BlogService} from '../../../services/blog.service';
import {ActivatedRoute,Router} from '@angular/router';    

@Component({
  selector: 'app-delete-blog',
  templateUrl: './delete-blog.component.html',
  styleUrls: ['./delete-blog.component.css']
})
export class DeleteBlogComponent implements OnInit {
  foundBlog=false;
  message;
  messageClass;
  processing=false;
  blog;
currentUrl;

  constructor(private blogService:BlogService,private router:Router,private activatedRoute:ActivatedRoute) { }


deleteBlog(){
  this.processing=false;
this.blogService.deletePost(this.currentUrl).subscribe((data)=>{
    if(!data.success){
      this.message=data.message;
      this.messageClass='alert alert-warning';
    }
    else{
      this.message=data.message;
      this.messageClass='alert alert-success';
        setTimeout(()=>{
        this.router.navigate(['/dashboard']);
      },2000);
    }

});
}


ngOnInit() {
  this.currentUrl=this.activatedRoute.snapshot.params.id
this.blogService.getBlock(this.currentUrl).subscribe(data=>{
  if(!data.blog){
    this.messageClass='alert alert-warning';
    this.message='Blog not Found !'
  }
  else
  {
 this.foundBlog=true;
 this.blog=data.blog;
  }
});
  }

}
