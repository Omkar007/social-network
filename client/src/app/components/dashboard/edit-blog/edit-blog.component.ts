import { Component, OnInit } from '@angular/core';
import {BlogService} from '../../../services/blog.service';
import {ActivatedRoute,Router} from '@angular/router';             //lib allows to grab active url 

@Component({
  selector: 'app-edit-blog',
  templateUrl: './edit-blog.component.html',
  styleUrls: ['./edit-blog.component.css']
})
export class EditBlogComponent implements OnInit {

message;
messageClass;
processing=false;
currentUrl;
loadEdit=false;
blog;
  constructor(private blogService:BlogService,private router:Router,private activatedRoute:ActivatedRoute ) { }

//edit button click
onEditSubmit(){
this.processing=true;
this.blogService.updateBlog(this.blog).subscribe(data=>{
  if(!data.success){
      this.message=data.message;
      this.messageClass='alert alert-warning';
  }
  else{
      this.message=data.message;
      this.messageClass='alert alert-success';
      setTimeout(()=>{
        this.router.navigate(['/dashboard']);
      },2000);
  }
});
}

goBack(){
  window.history.back();
}

  ngOnInit() {
this.currentUrl=this.activatedRoute.snapshot.params;
this.blogService.getBlock(this.currentUrl.id).subscribe(data=>{
  if(!data.blog){
    this.messageClass='alert alert-warning';
    this.message='Blog not Found !'
  }
  else
  {
 this.loadEdit=true;
 this.blog=data.blog;
  }
});
  }

}
