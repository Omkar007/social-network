import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../services/user-create-service.service'; 
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
    providers:[AuthenticationService]
})
export class NavbarComponent implements OnInit {

  constructor(private authService : AuthenticationService,
              private router: Router,private flashMsgService:FlashMessagesService) { }

  onLogoutClick(){
  this.authService.logout();
  this.flashMsgService.show('You are logged out !!',{cssClass:'alert-info'});
  this.router.navigate(['/']);
  }

  ngOnInit() {
  }

}
