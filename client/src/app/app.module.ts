//importing all modules 
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule,Routes} from '@angular/router';
import { AppComponent } from './app.component';
import { UserCreateComponent } from './components/user-create/user-create.component';
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './components/profile/profile.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { RegnavbarComponent } from './components/regnavbar/regnavbar.component';  
import { FlashMessagesModule } from 'angular2-flash-messages';
import { AuthGuard } from './guards/auth.guard';
import { NotAuthGuard } from './guards/notauth.guard';
import {AuthenticationService} from './services/user-create-service.service';
import { HomeComponent } from './components/home/home.component'; 
import {BlogService} from './services/blog.service';
import { EditBlogComponent } from './components/dashboard/edit-blog/edit-blog.component';
import { DeleteBlogComponent } from './components/dashboard/delete-blog/delete-blog.component';
import { PublicProfileComponent } from './components/public-profile/public-profile.component';
import { CommentComponent } from './components/comment/comment.component'

//defining all routes
const appRoutes: Routes = [
     { path: '', redirectTo: 'login', pathMatch:'full'},
     { path:'register', component: UserCreateComponent,canActivate:[NotAuthGuard]},
     {path:'home',component:HomeComponent,canActivate:[AuthGuard]},
     { path:'login', component: LoginComponent,canActivate:[NotAuthGuard]},
     { path:'dashboard', component: DashboardComponent,canActivate:[AuthGuard]},
     { path:'editBlog/:id', component: EditBlogComponent,canActivate:[AuthGuard]},
     { path:'deleteBlog/:id', component: DeleteBlogComponent,canActivate:[AuthGuard]},
     { path:'profile', component: ProfileComponent,canActivate:[AuthGuard]},
     {path: 'user/:username', component: PublicProfileComponent, canActivate: [AuthGuard] },
     { path: '**', component: LoginComponent }
    ];


@NgModule({
  declarations: [
    AppComponent,
    UserCreateComponent,
    LoginComponent,
    DashboardComponent,
    ProfileComponent,
    NavbarComponent,
    RegnavbarComponent,
    HomeComponent,
    EditBlogComponent,
    DeleteBlogComponent,
    PublicProfileComponent,
    CommentComponent
  ],
  imports: [
    BrowserModule,FormsModule,HttpModule,ReactiveFormsModule,CommonModule,FlashMessagesModule,
    RouterModule.forRoot(appRoutes)       //injecting all routes 
  ],
  providers: [AuthenticationService,BlogService,AuthGuard,NotAuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
