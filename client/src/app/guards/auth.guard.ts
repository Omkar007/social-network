import { Injectable }  from '@angular/core';
import {CanActivate, Router,ActivatedRouteSnapshot,RouterStateSnapshot}  from '@angular/router';
import { AuthenticationService }      from '../services/user-create-service.service';

@Injectable()
export class AuthGuard implements CanActivate {
  redirectUrl;
   constructor(private authService : AuthenticationService,private router:Router) { }

  canActivate(    router: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) {
    if(this.authService.loggedIn()){
      return true;
    }
    else{
      this.redirectUrl = state.url; // Grab previous url
      this.router.navigate(['/login']);
      return false;
    }
  }
}