import { Injectable }                 from '@angular/core';
import { CanActivate, Router}          from '@angular/router';
import { AuthenticationService }      from '../services/user-create-service.service';

@Injectable()
export class NotAuthGuard implements CanActivate {
   constructor(private authService : AuthenticationService,private router:Router) { }

  canActivate() {
    if(this.authService.loggedIn()){
         this.router.navigate(['/dashboard']);
          return false;
    }
    else{
      return true;
    }
  }
}