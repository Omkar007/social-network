import { Injectable } from '@angular/core';
import {AuthenticationService} from './user-create-service.service'
import 'rxjs/add/operator/map';
import {Http,Headers,RequestOptions} from '@angular/http';

@Injectable()
export class BlogService {

options;
domain=this.authService.domain;

  constructor(private authService:AuthenticationService,
              private http:Http) { }

//create auth headers
createAuthenticationHeaders(){
this.authService.loadToken();
  this.options = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'application/json', // Format set to JSON
        'authorization': this.authService.authToken // Attach token
      })
    });
}

//Service to call the create Blog API 
newBlog(blog){
    this.createAuthenticationHeaders();                 //passing headers as options
return this.http.post(this.domain+'/blogs/newBlog',blog,this.options).map(res=>res.json());
}

//service call to allblogs API
getAllBlogs(){
  this.createAuthenticationHeaders();  
  return this.http.get(this.domain+'/blogs/allBlogs',this.options).map(res=>res.json());
}

//get single block 
getBlock(id){
  this.createAuthenticationHeaders();
  return this.http.get(this.domain+'/blogs/singleBlog/'+id,this.options).map(res=>res.json());
}

//update edited blog
updateBlog(blog){
  this.createAuthenticationHeaders();
  return this.http.put(this.domain+'/blogs/updateBlog',blog,this.options).map(res=>res.json());
  
}

//delete blog 
deletePost(id){
 this.createAuthenticationHeaders();
  return this.http.delete(this.domain+'/blogs/deleteBlog/'+id,this.options).map(res=>res.json());
}

//like blog service
likeBlog(id){
  let blogData={id:id};
   this.createAuthenticationHeaders();
  return this.http.put(this.domain+'/blogs/likesBlog',blogData,this.options).map(res=>res.json());
}

//dislike blog service
dislikeBlog(id){
  let blogData={id:id};
   this.createAuthenticationHeaders();
  return this.http.put(this.domain+'/blogs/dislikeBlog',blogData,this.options).map(res=>res.json());
}

//comment post service
postNewComment(id,comment){
 this.createAuthenticationHeaders();
 let commentData={id:id,comment:comment};
 return this.http.post(this.domain+'/blogs/comment',commentData,this.options).map(res=>res.json());
}
}
