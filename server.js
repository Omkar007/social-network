
//importing modules 
var express=require('express');
var morgan=require('morgan');
var mongoose=require('mongoose');
var bodyParser = require('body-parser');
const cors=require('cors');
const path=require('path');
const userRoute = require('./app/routes/userRoutes'); 
const blogRoute = require('./app/routes/blogRoutes'); 
const config=require('./config/database');

//app setup
var app=express();
const port=process.env.PORT || 4000;

//using middleware to log the requests on console   (order should be maintained)
app.use(morgan('dev'));
//only with this domain are allowed to use this api (cors library)
app.use(cors({
  option:'http://localhost:4200',
  optionsSuccessStatus:200
}));
app.use(bodyParser.json());                              // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true }));      // for parsing application/x-www-form-urlencoded
app.use(express.static(__dirname+'/public'));            //folder having angular controllers,auths

//defining custom controller routing (backend routes)
app.use('/api', userRoute);
app.use('/blogs', blogRoute);

//connect to mongodb
mongoose.Promise = global.Promise;
mongoose.connect(config.uri,(err)=>{
if(err){
console.log("Error in connecting to Mongo DB "+err);
}
else{
console.log("Successfully connected to Mongo DB ");
}
});

//starting server on specified port
var server=app.listen(port,()=>{
  console.log('listening to requests on port '+port);
});

// route
app.get('*',(req,res)=>{        // * => whatever user types it serves this file
    res.sendFile(path.join(__dirname+'/public/app/views/index.html'));
});
